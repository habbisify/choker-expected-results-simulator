# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 22:10:46 2020

@author: Joni Seppälä (joni.seppala1@gmail.com)
"""

'''
The purpose of this program is to help me and others to understand which
hands dealt in the choker game are preferable, and which ones to avoid. It
simulates the real game's hand dealing process in detail; demotions and
promotions are taken into account. After the cards are dealt the resulting
material imbalances are considered and an evaluation of the expected result
is assigned with the hands. After many iterations the expected results should
converge to real life expected values for every possible hand.

Assumptions:
    Card values:
        P=1, N=3, B=3, R=5, Q=9
    Expected results for material imbalances:
        imbalance -> expected result
        0 -> 0
        1 -> 0.198
        2 -> 0.358
        3 -> 0.556
        4 -> 0.713
        5 -> 0.875
        6+ -> 1

Please note that this is a personal freetime project and mistakes are possible.
If you have additional ideas or fixes to implement to this program, please
contact me via email.

From https://play.google.com/store/apps/details?id=com.queenloop.choker :

"- Bring your poker face. Bring your best chess. Bring everything.
CHOKER™ - the ultimate game!

Ready for high stakes chess? For the poker phase, you’ll need cunning and cool
calculation. In chess, you'll need strategy and brilliance. To win the pot,
you need it all."
'''

import time
import collections
from random import shuffle

## Parameters

# Verbosity parameter
suppress_intermediate_prints = True

# Amount how many hands are simulated. The default is int(10e4).
iterations = 10e6

# Amount how many cards are considered (out of 5). Initially dealt cards are
# prioritized. The default is 5.
n = 5

# Should the permutations be considered the same or not. The default is True.
ordered = True

# Evaluation for each piece. These can be modified for personal preferences.
VAL_P = 1
VAL_N = 3
VAL_B = 3.5
VAL_R = 5
VAL_Q = 9

# Representation symbol for each piece.
# These can also be modified for personal preferences.
SYM_P = "♟︎"
SYM_N = "♞"
SYM_B = "♝"
SYM_R = "♜"
SYM_Q = "♛"


def eval_material_expectation(imbalance):
    x = imbalance / 10 # Normalization of the imbalance for the formula
    if x > 1:
        return 1
    elif x < -1:
        return -1
    
    # The formula works as intended between values [-1, 1]
    expectation = 2*x - 2*x**3 + 4*x**5
    
    if expectation > 1:
        expectation = 1
    if expectation < -1:
        expectation = -1

    return expectation


class Card:
    def __init__(self, i):
        values = [VAL_P, VAL_N, VAL_B, VAL_R, VAL_Q]
        symbols = [SYM_P, SYM_N, SYM_B, SYM_R, SYM_Q]
        self.value = values[i]
        self.symbol = symbols[i]
        self.changed = 0 # 0 = not changed; 1 = promoted; -1 = demoted


class Deck:
    def __init__(self):
        self.cards = []
        # 16 pawns, 8 knights, 8 bishops, 8 rooks, 4 queens
        for i in [(0,16), (1,8), (2,8), (3,8), (4,4)]:
            for _ in range(i[1]):
                self.cards.append(Card(i[0]))
        shuffle(self.cards) # shuffle makes the deck random

    def draw_random(self):
        # Return a card and remove it from the deck simultaneously
        return self.cards.pop()


class Player:
    def __init__(self):
        self.cards = []

    def resolve_demotions(self):
        top_card = self.cards[-1]
        symbols = self.symbols_cards()
        # 3/3/3/2 knights/bishops/rooks/queens results in demotion
        demotables = [(SYM_N, 3), (SYM_B, 3), (SYM_R, 3), (SYM_Q, 2)]
        for d in demotables:
            if top_card.symbol == d[0] and symbols.count(d[0]) >= d[1]:
                top_card.value = VAL_P
                top_card.changed = -1

    def resolve_promotions(self):
        top_card = self.cards[-1]
        symbols = self.symbols_cards()
        unique_symbols = list(set(symbols))

        # pawns
        if top_card.symbol == SYM_P:
            if symbols.count(SYM_P) == 3:
                top_card.value = VAL_R
                top_card.changed = 1
            elif symbols.count(SYM_P) > 3:
                top_card.value = VAL_Q
                top_card.changed = 1

        if len(self.cards) == 5:

            # palace
            if len(unique_symbols) == 5:
                p_index = symbols.index(SYM_P)
                self.cards[p_index].value = VAL_Q
                self.cards[p_index].changed = 1

            # empress
            if len(unique_symbols) == 2:
                empress = True
                three_cards = []
                if symbols.count(unique_symbols[0]) == 3:
                    three_symbol = unique_symbols[0]
                elif symbols.count(unique_symbols[1]) == 3:
                    three_symbol = unique_symbols[1]
                else:
                    # 4-1 hand, not an empress
                    empress = False

                if empress:
                    for card in self.cards:
                        if card.symbol == three_symbol:
                            three_cards.append(card)
                    changed_threes = []
                    for card in three_cards:
                        changed_threes.append(card.changed)
                    if -1 in changed_threes:
                        # Prioritize a demoted piece
                        c_index = changed_threes.index(-1)
                        three_cards[c_index].value = VAL_Q
                        three_cards[c_index].changed = 1
                    elif 1 in changed_threes:
                        # Prioritize promotion for pawn that is already promoted to a rook
                        c_index = changed_threes.index(1)
                        three_cards[c_index].value = VAL_Q
                        three_cards[c_index].changed = 1
                    else:
                        # No prioritizing
                        three_cards[0].value = VAL_Q
                        three_cards[0].changed = 1

    def sum_cards(self):
        summed = 0
        for card in self.cards:
            summed += card.value
        return summed
    
    def symbols_cards(self):
        symbols = []
        for card in self.cards:
            symbols.append(card.symbol)
        return symbols

    def print_cards(self):
        print("Player cards: ")
        for card in self.cards:
            print(card.symbol,card.value)
        print("Sum: {}".format(self.sum_cards()))


def assign_cards():
    deck = Deck()
    players = [Player(), Player()]
    
    for i in range(5):
        for player in players:
            player.cards.append(deck.draw_random())
            player.resolve_demotions()
            player.resolve_promotions()
    
    p1_sum = players[0].sum_cards()
    p2_sum = players[1].sum_cards()
    imbalance = p1_sum - p2_sum
    expectation = eval_material_expectation(imbalance)
    if not suppress_intermediate_prints:
        print("Expectation: {}".format(expectation))
        for player in players:
            player.print_cards()

    return players, expectation


def calculate_expectation(iterations: int = int(10e4), n: int = 5, ordered: bool = True) -> None:
    '''
    Calculate and print the expected result for all hands, based on the
    simulated runs.

    Parameters
    ----------
    iterations : int, optional
        Amount how many hands are simulated. The default is int(10e4).
    n : int, optional
        Amount how many cards are considered (out of 5). Initially dealt cards
        are prioritized. The default is 5.
    ordered : bool, optional
        Should the permutations be considered separately or not. The default is
        True.

    Returns
    -------
    None

    '''
    hands = {}
    for i in range(iterations):
        players, expectation = assign_cards()
        for i in range(2):
            if i == 1:
                # Second hand: Expectation is inverse w.r.t. first one
                expectation *= -1
            # Make n first cards as the dict key
            hand = players[i].symbols_cards()[0:n]
            if ordered:
                # Permutations are considered the same
                hand.sort()
            hand = tuple(hand) # dict does not accept list as a key
            if not hand in hands:
                hands[hand] = [expectation, 1]
                # The latter value is the amount of observations of the hand
            else:
                hands[hand][0] += expectation
                hands[hand][1] += 1

    # Order hands alphabetically.
    # - This could also be done w.r.t. piece values
    # - Or by hand's expected value
    # - Or by hand's prevalence
    hands = collections.OrderedDict(sorted(hands.items()))

    for hand, value in hands.items():
        expectation_avg = value[0] / value[1]
        for card in hand:
            print(card, end=" ")
        print("{:.4f}".format(expectation_avg))
    print("Total unique hands: {}".format(len(hands)))


if __name__ == "__main__":
    start = time.time()
    calculate_expectation(int(iterations), n, ordered)
    end = time.time()
    print("\nTime elapsed (seconds): {:.1f}".format(end - start))
