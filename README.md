# About

**The purpose of [this](expectations.py) program is to help me and others to understand which
hands dealt in the choker game are preferable, and which ones to avoid.** It
simulates the real game's hand dealing process in detail; demotions and
promotions are taken into account. After the cards are dealt the resulting
material imbalances are considered and an evaluation of the expected result
is assigned with the hands. After many iterations the expected results should
converge to real life expected values for every possible hand.

## Assumptions:
### Card values:
* P = 1
* N = 3
* B = 3.5 (usually bishops are evaluated as 3 pawns, but since the game is (almost) always open, value of 3.5 can be reasoned with)
* R = 5
* Q = 9
### Expected results for material imbalances:
imbalance -> expected result (-1 = certain loss; 0 = indifferent; 1 = certain victory)
* 0 -> 0
* 1 -> 0.198
* 2 -> 0.358
* 3 -> 0.556
* 4 -> 0.713
* 5 -> 0.875
* 6+ -> 1

## Results

The obtained results can be found in the [results.txt](results.txt) file. They were generated
using 10 million iterations.

## Contact me

Please note that this is a personal freetime project and mistakes are possible.
If you have additional ideas or fixes to implement to this program, please
contact me via email: joni.seppala1@gmail.com.

## About the game

From https://play.google.com/store/apps/details?id=com.queenloop.choker :

> Bring your poker face. Bring your best chess. Bring everything.
CHOKER™ - the ultimate game!  
Ready for high stakes chess? For the poker phase, you’ll need cunning and cool
calculation. In chess, you'll need strategy and brilliance. To win the pot,
you need it all.